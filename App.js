import React, { Component } from 'react';
import { register_screens } from "./pages/screens";
import { Provider, connect } from "react-redux";
import { Navigation } from 'react-native-navigation';


import store from "./store";

console.log("store = ", store);
console.log("Provider = ", Provider);

register_screens(store, Provider);

export default class App extends Component {

	componentWillMount() {

	}

	constructor(props) {
		super(props);

		Navigation.startSingleScreenApp({
			screen: {
				screen: 'inkind.test.BreedList',
				title: 'Breed List',
				navigatorStyle: {
					statusBarTextColorScheme: "light",
					navBarHidden: false
				}
			},
			appStyle: {
				navBarBackgroundColor: "#2d324c",
				statusBarTextColorScheme: "light",
				navBarTextColor: "#ffffff",
				navBarButtonColor: "#ffffff",
				navBarNoBorder: true
			}
		});

	}
}


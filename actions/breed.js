import * as types from './types'
import Api from '../lib/api';

export function apiBreedList(){
    console.log('myactions - getList');
    return (dispatch, getState) => {
        return Api.get('breeds/list/all',{}).then(json => {
            //Parse response
            const arr = Object.keys(json.message).map( (key) => { return {key: key, text: key} });
            dispatch(setBreedList({list: arr}));
        }).catch(((ex) => {
            console.log(ex);
            dispatch(showError({error: json.message}));
        }))

    };
}

export function showError({error}){
    return {
        type: types.SHOWERROR,
        data: error
    };
}

export function setBreedList({list}){
    return {
        type: types.SET_BREEDLIST,
        breedlist: list
    };

}

export function selectBreed(breed){
    return {
        type: types.SELECT_BREED,
        breed: breed
    }
}

export function apiBreedImage(breed){
    console.log('myactions - apiBreedImage');
    return (dispatch, getState) => {
        return Api.get('breed/'+breed+'/images/random',{}).then(json => {
            //Parse response
            dispatch(selectBreedImage({image: json.message}));
        }).catch(((ex) => {
            console.log(ex);
            dispatch(showError({error: json.message}));
        }))

    };
}

export function selectBreedImage({image}){
    return {
        type: types.SET_BREEDIMAGE,
        image: image
    }
}



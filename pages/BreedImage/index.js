import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../../actions';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';

class BreedImage extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount() {
      this.props.apiBreedImage(this.props.breeditem.key);
    }

    render() {
        return ( typeof this.props.breedimage === 'string' ? 
          <View style={styles.mainWrapper}>
            <Image
            style={styles.dogImage}
            source={this.props.breedimage && {uri: this.props.breedimage}}
            />
          </View>
          :
          <View style={styles.mainWrapper}>
            <Text>
              Loading...
            </Text>
          </View>
        );
    }
}

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    paddingTop: 100
  },
	dogImage: {
		width: 200,
		height: 200,
    marginBottom: 50
	}
});
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
  }
  
  export default connect((state) => {  return {
    breedimage: state.breedimage,
    breeditem: state.breeditem
  }; },
   mapDispatchToProps)(BreedImage);

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../../actions';
import { ListItem } from 'react-native-elements'
import {
  StyleSheet,
  View,
  Text,
  FlatList,
} from 'react-native';

class BreedList extends Component {
    constructor(props){
        super(props);
        this.state = {loading: true};
    }

    componentDidMount() {
        this.props.apiBreedList().then(()=>{
          this.state = {loading: false}
        });
    }

    itemClicked(item){
        this.props.selectBreed(item);
    
        this.props.navigator.push({
          screen: 'inkind.test.BreedImage',
          title: item.text,
        });
    }
    
    render() {
        return Array.isArray(this.props.breedlist) ? (
          <FlatList
            data={this.props.breedlist}
            //renderItem={({item}) => <Text>{item.key}</Text>}
            renderItem={({item}) => <ListItem title={item.text} onPress={() => {this.itemClicked(item)}}/>}
          />
        ) : (
          <View style={styles.mainWrapper}>
            <Text>
              Loading...
            </Text>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    mainWrapper: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: '100%',
      backgroundColor: '#ffffff',
      paddingTop: 100
    }
  });
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
  }
  
  export default connect((state) => {  return {
    breedlist: state.breedlist
  }; },
   mapDispatchToProps)(BreedList);

import { Navigation } from 'react-native-navigation';
import Main from "./Main";
import BreedList from "./BreedList";
import BreedImage from "./BreedImage";

export function register_screens(store, Provider) {

  Navigation.registerComponent(
    'inkind.test.Main',
    () => Main,
    store,
    Provider
  );

  Navigation.registerComponent(
    'inkind.test.BreedList',
    () => BreedList,
    store,
    Provider
  );

  Navigation.registerComponent(
    'inkind.test.BreedImage',
    () => BreedImage,
    store,
    Provider
  );

};

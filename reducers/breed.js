
import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const breedlist = createReducer({}, {
    [types.SET_BREEDLIST](state, action) {
        return action.breedlist;
    },
    });
  

export const breeditem = createReducer({}, {
    [types.SELECT_BREED](state, action) {
        return action.breed;
    },
    });

export const breedimage = createReducer({}, {
        [types.SET_BREEDIMAGE](state, action) {
            return action.image;
        },
        [types.SELECT_BREED](state, action) {
            return {};
        },
    });
                            
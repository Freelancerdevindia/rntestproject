import { combineReducers } from 'redux';
import dogreducers from './dog'
import * as breedreducers from './breed'

export default combineReducers(Object.assign(
    dogreducers,
    breedreducers,
));
